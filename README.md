# About

WIP repository for creating a quickstart Spark/Scala project that can be quickly cloned and adapted to various Kaggle challenges, leaving the user to focus more on the data and as little as possible on config and pipelines.
 

## High-level - modules

To do (_breakdown in Zenhub cards_):
- [**Config - Data [Epic]**](https://github.com/smilenstoychev/kaggle-quickstart-dl4j-spark/issues/18) `/spec`
  - [x] Data Schemas (including defaults)
- [**Config - Environment [Epic]**](https://github.com/smilenstoychev/kaggle-quickstart-dl4j-spark/issues/17) `/config`
  - [x] Spark session variables - RAM, cores, etc
  - [x] Default paths
- [**Data Science [Epic]**](https://github.com/smilenstoychev/kaggle-quickstart-dl4j-spark/issues/5) - `/datascience`
  - [x] Quality Analysis (generic)
  - [x] Data Analysis (generic)
  - [ ] Cleaning, filtering (custom - filled-in by user)
  - [ ] Transformation (custom - filled-in by user)
- [**Machine Learning [Epic]**](https://github.com/smilenstoychev/kaggle-quickstart-dl4j-spark/issues/20) - `/machinelearning`
  - [ ] Transforming structured data in numeric form (generic)
  - [ ] Model training and evaluation pipelines (generic)
  - [ ] Model specification (custom - filled-in by user)
- [**Utilities [Epic]**](https://github.com/smilenstoychev/kaggle-quickstart-dl4j-spark/issues/8) - anything not extendable - `/utils`
  - [x] Spark session variable
  - [ ] Read data into DataFrames, etc functions
  - [ ] Write model/model versions
  - [ ] Kaggle console API for uploading results to challenge 
  - [ ] Implicits
 