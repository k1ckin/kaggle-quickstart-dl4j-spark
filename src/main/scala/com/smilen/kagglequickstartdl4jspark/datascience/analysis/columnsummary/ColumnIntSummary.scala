package com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnsummary

import com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnanalysis.ColumnWholeNumberAnalysis

case class ColumnIntSummary(
  analysis: ColumnWholeNumberAnalysis) extends ColumnWholeNumberSummary(analysis) {

  override val columnType: String = "Integer"
}
