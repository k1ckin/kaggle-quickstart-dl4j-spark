package com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnanalysis

import org.apache.spark.sql.types.StructField

case class ColumnTimestampAnalysis
(override val columnStruct: StructField)
  extends ColumnDataAnalysis {
}
