package com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnsummary

import com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnanalysis.ColumnDataAnalysis

abstract class BaseColumnDataSummary(
  analysis: ColumnDataAnalysis
) extends ColumnDataSummary {

  override val columnName: String = analysis.columnStruct.name
  override val columnType: String = analysis.columnStruct.dataType.toString
  override val reportFields: Seq[SummaryField] = Seq( )
}
