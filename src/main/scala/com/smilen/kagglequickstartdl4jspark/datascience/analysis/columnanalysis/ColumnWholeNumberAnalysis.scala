package com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnanalysis

import org.apache.spark.sql.types.StructField

case class ColumnWholeNumberAnalysis(
  override val columnStruct: StructField,
  max: Long,
  min: Long,
  mean: Double,
  stdev: Double,
  median: Long
) extends ColumnDataAnalysis
