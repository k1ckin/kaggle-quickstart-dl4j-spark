package com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnsummary

case class SummaryField(key: String, value: String, flag: Boolean = false) {

  def this(key: String, value: Any, flag: Boolean) = this(key, value.toString, flag)
}
