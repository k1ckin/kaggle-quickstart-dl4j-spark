package com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnsummary

import com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnanalysis._
import org.apache.spark.sql.types.DataTypes

object ColumnDataSummaryFunctions {

  def getDataSummaryForAnalysis(dataAnalysis: ColumnDataAnalysis): ColumnDataSummary = dataAnalysis match {
    case d: ColumnWholeNumberAnalysis => getWholeNumberSummary(d)
    case d: ColumnStringAnalysis => ColumnStringSummary(d)
    case d: ColumnDecimalNumberAnalysis => getDecimalNumberSummary(d)
    case d: ColumnTimestampAnalysis => ColumnTimestampSummary(d)
    case d: ColumnBooleanAnalysis => ColumnBooleanSummary(d)
    case _ => ???
  }

  private def getWholeNumberSummary(
    dataAnalysis: ColumnWholeNumberAnalysis): ColumnWholeNumberSummary = dataAnalysis.columnStruct.dataType match {
    case DataTypes.IntegerType => ColumnIntSummary(dataAnalysis)
    case DataTypes.LongType => ColumnLongSummary(dataAnalysis)
  }

  private def getDecimalNumberSummary(
    dataAnalysis: ColumnDecimalNumberAnalysis
  ): ColumnDecimalNumberSummary = dataAnalysis.columnStruct.dataType match {
    case DataTypes.DoubleType => ColumnDoubleSummary(dataAnalysis)
    case DataTypes.FloatType => ColumnFloatSummary(dataAnalysis)
  }
}
