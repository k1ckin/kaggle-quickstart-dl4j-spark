package com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnanalysis

import org.apache.spark.sql.types.StructField

case class ColumnBooleanAnalysis(
  override val columnStruct: StructField,
  numberOfTrue: Long,
  numberOfFalse: Long,
  percentageTrue: Double
)
  extends ColumnDataAnalysis {
}
