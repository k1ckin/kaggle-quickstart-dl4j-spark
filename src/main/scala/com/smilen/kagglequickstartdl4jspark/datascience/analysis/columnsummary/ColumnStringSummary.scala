package com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnsummary

import com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnanalysis.ColumnStringAnalysis

case class ColumnStringSummary
(analysis: ColumnStringAnalysis) extends BaseColumnDataSummary(analysis) {

  override val reportFields: Seq[SummaryField] = Seq( )
}
