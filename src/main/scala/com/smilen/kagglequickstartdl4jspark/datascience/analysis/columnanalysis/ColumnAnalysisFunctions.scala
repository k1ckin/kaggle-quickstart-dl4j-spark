package com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnanalysis

import com.smilen.kagglequickstartdl4jspark.utils.implicits._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{DataTypes, LongType}
import org.apache.spark.sql.{DataFrame, Row}

object ColumnAnalysisFunctions {

  def getDataAnalysisForFirstColumn(df: DataFrame): ColumnDataAnalysis = df.schema.fields.head.dataType match {
    case DataTypes.IntegerType | DataTypes.LongType => getDataAnalysisForWholeNumber(df)
    case DataTypes.FloatType | DataTypes.DoubleType => getDataAnalysisForDecimalNumber(df)
    case DataTypes.TimestampType => getDataAnalysisForTimestamp(df)
    case DataTypes.StringType => getDataAnalysisForString(df)
    case DataTypes.BooleanType => getDataAnalysisForBoolean(df)
    case _ => ??? // TODO(Add default)
  }

  def getDataAnalysisForString(df: DataFrame): ColumnStringAnalysis = {
    val groupedByString = df.groupBy(df.firstCol( )).agg(count("*").alias("count"))
    val numberOfDistinct = groupedByString.count( )
    val emptyStrings = groupedByString.filter(df.firstCol( ) === lit("")).select("count").head( ).getLong(0)
    val dfOfLengths = df.withColumn("length", length(df.firstCol( ))).select("length")
    val analysisOfLengths = getDataAnalysisForWholeNumber(dfOfLengths)
    val mostOccurring = groupedByString.orderBy(desc("count")).head(2).toList.map(r => (r.getString(0), r.getLong(1)))

    ColumnStringAnalysis(df.firstStruct( ), numberOfDistinct, mostOccurring, emptyStrings, analysisOfLengths)
  }

  def getDataAnalysisForWholeNumber(df: DataFrame): ColumnWholeNumberAnalysis = {
    val stats: Row = df.agg(max(df.firstCol).cast(LongType), min(df.firstCol).cast(LongType), avg(df.firstCol))
      .collect( ).head
    ColumnWholeNumberAnalysis(df.firstStruct, stats.getLong(0), stats.getLong(1), stats.getDouble(2), stats.getDouble
    (2), stats.getLong(0))
  }

  def getDataAnalysisForTimestamp(df: DataFrame): ColumnTimestampAnalysis = ??? // TODO(Add Timestamp support)

  def getDataAnalysisForBoolean(df: DataFrame): ColumnBooleanAnalysis = ??? // TODO(Add Boolean support)

  def getDataAnalysisForDecimalNumber(df: DataFrame): ColumnDecimalNumberAnalysis = ??? // TODO(Add Number support)
}
