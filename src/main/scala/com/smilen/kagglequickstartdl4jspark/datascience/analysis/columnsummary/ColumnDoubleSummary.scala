package com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnsummary

import com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnanalysis.ColumnDecimalNumberAnalysis

case class ColumnDoubleSummary(
  analysis: ColumnDecimalNumberAnalysis) extends ColumnDecimalNumberSummary(analysis) {

  override val columnType: String = "Decimal"
}
