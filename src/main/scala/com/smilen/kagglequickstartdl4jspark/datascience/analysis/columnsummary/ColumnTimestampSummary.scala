package com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnsummary

import com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnanalysis.ColumnDataAnalysis

case class ColumnTimestampSummary(
  analysis: ColumnDataAnalysis)
  extends BaseColumnDataSummary(analysis) {
}
