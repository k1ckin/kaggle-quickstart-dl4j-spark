package com.smilen.kagglequickstartdl4jspark.datascience.analysis.printing.console

import com.smilen.kagglequickstartdl4jspark.datascience.analysis.DataReport
import com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnsummary.{ColumnDataSummary, SummaryField}
import com.smilen.kagglequickstartdl4jspark.utils.Tabulator

import scala.Console.{RED, WHITE}

object ReportingToConsole {

  type ConsolePrintableReport = Seq[Seq[Seq[String]]]

  def printConsolePrintableReport(report: ConsolePrintableReport): Unit = {
    report
      .map(Tabulator.format)
      .foreach(println)
  }

  def printDataReport(dataReport: DataReport): Unit = {
    val report = convertDataReportToConsolePrintableReport(dataReport)
    printConsolePrintableReport(report)
  }

  def convertDataReportToConsolePrintableReport(dataReport: DataReport): ConsolePrintableReport = {
    val columnReports = dataReport.columnStatisticalAnalysis.map(convertColumnReportToConsolePrintable)
    columnReports
  }

  def convertColumnReportToConsolePrintable(columnDataReport: ColumnDataSummary): Seq[Seq[String]] = {
    val columnNames: List[String] = columnDataReport.reportFields.map(convertReportFieldKeyToConsolePrintable).toList
    val columnContent: List[String] = columnDataReport.reportFields.map(convertReportFieldValueToConsolePrintable)
      .toList
    val header: Seq[String] = "Name" :: "Type" :: columnNames
    val content: Seq[String] = columnDataReport.columnName :: columnDataReport.columnType :: columnContent
    header :: content :: Nil
  }

  private def convertReportFieldKeyToConsolePrintable(reportField: SummaryField): String = if (reportField.flag)
    wrapRed(reportField.key)
  else reportField.key

  private def convertReportFieldValueToConsolePrintable(
    reportField: SummaryField): String = if (reportField.flag) wrapRed(reportField.value)
  else reportField.value

  private def wrapRed = wrapInColor(RED)

  private def wrapInColor(color: String) = (text: String) => s"$color$text$WHITE"
}
