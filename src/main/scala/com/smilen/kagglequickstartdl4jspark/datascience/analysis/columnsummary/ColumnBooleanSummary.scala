package com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnsummary

import com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnanalysis.ColumnBooleanAnalysis

case class ColumnBooleanSummary(analysis: ColumnBooleanAnalysis) extends BaseColumnDataSummary(analysis) {
}
