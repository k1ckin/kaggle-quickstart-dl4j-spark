package com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnanalysis

import org.apache.spark.sql.types.StructField

trait ColumnDataAnalysis {

  val columnStruct: StructField
}
