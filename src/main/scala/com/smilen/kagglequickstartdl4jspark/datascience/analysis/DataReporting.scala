package com.smilen.kagglequickstartdl4jspark.datascience.analysis

import com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnanalysis.ColumnAnalysisFunctions
.getDataAnalysisForFirstColumn
import com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnsummary.ColumnDataSummaryFunctions
.getDataSummaryForAnalysis
import org.apache.spark.sql.DataFrame

object DataReporting {

  def getDataReportForDataframe(df: DataFrame): Unit = {
    val columnReports = df.columns
      .map(c => df.select(c))
      .map(getDataAnalysisForFirstColumn)
      .map(getDataSummaryForAnalysis)
    DataReport(columnReports)
  }
}
