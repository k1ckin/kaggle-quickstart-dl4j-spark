package com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnsummary

trait ColumnDataSummary {

  val reportFields: Seq[SummaryField]
  val columnName: String
  val columnType: String

  implicit def tupleToField(tuple: (String, Any)): SummaryField = new SummaryField(tuple._1, tuple._2, false)
}
