package com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnanalysis

import org.apache.spark.sql.types.StructField

case class ColumnStringAnalysis(
  override val columnStruct: StructField,
  numberOfDistinct: Long,
  mostOccurring: Seq[(String, Long)],
  emptyStrings: Long,
  stringLengthAnalysis: ColumnWholeNumberAnalysis
) extends ColumnDataAnalysis
