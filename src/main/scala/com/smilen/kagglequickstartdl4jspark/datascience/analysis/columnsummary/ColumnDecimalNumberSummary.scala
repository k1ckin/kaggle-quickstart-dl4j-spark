package com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnsummary

import com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnanalysis.ColumnDecimalNumberAnalysis

abstract class ColumnDecimalNumberSummary(
  analysis: ColumnDecimalNumberAnalysis) extends BaseColumnDataSummary(analysis) {
}
