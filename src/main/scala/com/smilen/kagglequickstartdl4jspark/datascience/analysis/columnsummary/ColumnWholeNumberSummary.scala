package com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnsummary

import com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnanalysis.ColumnWholeNumberAnalysis

abstract class ColumnWholeNumberSummary(
  analysis: ColumnWholeNumberAnalysis
) extends BaseColumnDataSummary(analysis) {

  override val reportFields: Seq[SummaryField] = Seq(
    ("Max", analysis.max),
    ("Min", analysis.min)
  )
}
