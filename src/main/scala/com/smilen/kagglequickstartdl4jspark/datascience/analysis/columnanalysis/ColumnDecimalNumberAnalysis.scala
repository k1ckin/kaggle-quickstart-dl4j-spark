package com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnanalysis

import org.apache.spark.sql.types.StructField

case class ColumnDecimalNumberAnalysis(
  override val columnStruct: StructField,
  max: Double,
  min: Double,
  mean: Double,
  stDev: Double,
  median: Double
)
  extends ColumnDataAnalysis {
}
