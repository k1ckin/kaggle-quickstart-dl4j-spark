package com.smilen.kagglequickstartdl4jspark.datascience.analysis

import com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnsummary.ColumnDataSummary

case class DataReport(columnStatisticalAnalysis: Seq[ColumnDataSummary]) {
}
