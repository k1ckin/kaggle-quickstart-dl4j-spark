package com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnsummary

import com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnanalysis.ColumnWholeNumberAnalysis

case class ColumnLongSummary(
  wholeNumberAnalysis: ColumnWholeNumberAnalysis) extends ColumnWholeNumberSummary(wholeNumberAnalysis) {

  override val columnType: String = "Long"
}
