package com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnsummary

import com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnanalysis.ColumnDecimalNumberAnalysis

case class ColumnFloatSummary(
  analysis: ColumnDecimalNumberAnalysis) extends ColumnDecimalNumberSummary(analysis) {

  override val columnType: String = "Float"
}
