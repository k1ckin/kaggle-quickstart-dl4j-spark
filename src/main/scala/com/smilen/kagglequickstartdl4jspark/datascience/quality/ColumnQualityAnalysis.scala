package com.smilen.kagglequickstartdl4jspark.datascience.quality

import org.apache.spark.sql.types.StructField

case class ColumnQualityAnalysis(
  field: StructField,
  nulls: Long,
  invalid: Long
) {
}
