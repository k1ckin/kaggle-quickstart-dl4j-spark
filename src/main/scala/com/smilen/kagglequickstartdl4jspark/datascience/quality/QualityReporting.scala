package com.smilen.kagglequickstartdl4jspark.datascience.quality

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.{lit, udf}
import org.apache.spark.sql.types.StructField

object QualityReporting {

  def getQualityReport(df: DataFrame): QualityReport = {
    val total = df.count
    val columnReports = ColumnReporting.getColumnReportsForData(df, total)

    QualityReport(columnReports, total)
  }

  private def isColumnValueValidForTypeUDF(structField: StructField) = udf(
    (col: String) => ColumnTypeValidation
      .isValueValidForType
      (col, structField))

  object ColumnReporting {

    def getColumnReportsForData(df: DataFrame, total: Long): List[ColumnQualityReport] = {
      val columnAnalysisResults = getColumnAnalysisForDataFrame(df)
      columnAnalysisResults.map(ColumnQualityReport(_, total))
    }

    def getColumnAnalysisForDataFrame(df: DataFrame): List[ColumnQualityAnalysis] =
      df.columns.toList
        .map(df.col)
        .map(c => getColumnAnalysisForFirstColumn(df.select(c)))

    def getColumnAnalysisForFirstColumn(df: DataFrame, struct: StructField = null): ColumnQualityAnalysis = {
      val firstColumnStruct = if (struct != null) struct else df.schema.fields.head
      val typeValidationFunction = isColumnValueValidForTypeUDF(firstColumnStruct)

      val columnSelector = df.col(df.columns.head)

      val nulls = df.filter(columnSelector.isNull).count( )
      val invalid = df
        .filter(columnSelector.isNotNull)
        .filter(typeValidationFunction(columnSelector) === lit(false))
        .count( )

      ColumnQualityAnalysis(firstColumnStruct, nulls, invalid)
    }
  }

  object RowReporting {
  }

}
