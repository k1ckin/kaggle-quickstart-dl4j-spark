package com.smilen.kagglequickstartdl4jspark.datascience.quality

case class ColumnQualityReport(private val analysis: ColumnQualityAnalysis, total: Long) {

  val columnName: String = analysis.field.name
  val columnType = analysis.field.dataType
  val nulls: Long = analysis.nulls
  val invalid: Long = analysis.invalid

  val percentageValid: Double = if (total != 0) (total - nulls - invalid).toDouble / total.toDouble else 0
}
