package com.smilen.kagglequickstartdl4jspark.datascience.quality

import scala.Console.{RED, WHITE}

case class QualityReport(
  columnReports: List[ColumnQualityReport],
  totalRecords: Long
) {

  override def toString: String = s"Total Records: ${totalRecords}"

  def getListOfColumnReportsPerFieldType() = {

    val columnNames = s"${WHITE}Column Name" :: columnReports.map(_.columnName)
    val columnTypes = "Data Type" :: columnReports.map(_.columnType.toString.replaceFirst("Type", ""))
    val total = "Total" :: List.fill(columnReports.length)(totalRecords.toString)
    val columnNulls = "Nulls" :: columnReports.map(c => conditionalRedWrap(c.nulls.toString, c.nulls > 0))
    val columnInvalids = "Invalid" :: columnReports.map(_.invalid.toString)
    val columnPercentages: Seq[String] = "Valid (%)" :: columnReports.map(_.percentageValid.formatted("%.4f"))

    columnNames :: columnTypes :: total :: columnNulls :: columnInvalids :: columnPercentages :: Nil
  }

  private def conditionalRedWrap(text: String, condition: => Boolean): String = if (condition) wrapRed(text) else text

  private def wrapRed = wrapInColor(RED)

  private def wrapInColor(color: String) = (text: String) => s"$color$text$WHITE"
}
