package com.smilen.kagglequickstartdl4jspark.datascience.quality

import org.apache.spark.sql.types.{DataTypes, StructField}

object ColumnTypeValidation {

  val isValueValidForType: (String, StructField) => Boolean = (
  value: String, structField: StructField) => structField.dataType match {

    case DataTypes.BooleanType => isStringBoolean(value)
    case DataTypes.IntegerType | DataTypes.LongType => isStringIntOrLong(value)
    case DataTypes.DoubleType | DataTypes.FloatType => isStringDoubleOrFloat(value)
    case DataTypes.StringType => isStringValid(value)
    case DataTypes.TimestampType => isStringTimestamp(value)
    case _ => false
  }

  def isStringBoolean(s: String): Boolean = booleanValues contains s.toLowerCase

  private def booleanValues = "true" :: "false" :: "0" :: "1" :: Nil

  def isStringDoubleOrFloat(s: String) = s.matches("[-+]?[0-9]*(\\.[0-9]+)?")

  def isStringIntOrLong(s: String): Boolean = s.matches("[+-]?(\\d+)")

  def isStringValid(s: String) = s.trim.nonEmpty

  def isStringTimestamp(s: String): Boolean = true
}
