package com.smilen.kagglequickstartdl4jspark.utils

import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, Dataset, Encoder}
import Spark.{sparkSession => spark}
import com.smilen.kagglequickstartdl4jspark.config.DefaultPaths._
import com.smilen.kagglequickstartdl4jspark.specs.DataConfig._
import spark.implicits._

object Utils {

  def readTrainCSVToDataset[T: Encoder](): Dataset[T] = readCSVToDatasetWithStruct(trainCSVPath, trainSchema)

  def readCSVToDatasetWithStruct[T: Encoder](path: String, struct: StructType): Dataset[T] =
    spark.read
      .option("header", "true")
      .schema(struct)
      .csv(path)
      .as[T]

  def readTrainCSVToDataFrame(): DataFrame = readCSVToDataFrameWithStruct(trainCSVPath, trainSchema)

  def readCSVToDataFrameWithStruct(path: String, struct: StructType): DataFrame =
    spark.read
      .option("header", "true")
      .schema(struct)
      .csv(path)
}
