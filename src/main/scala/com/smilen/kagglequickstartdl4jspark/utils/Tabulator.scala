package com.smilen.kagglequickstartdl4jspark.utils

// from https://stackoverflow.com/questions/7539831/scala-draw-table-to-console
object Tabulator {

  val minWidth = 10

  def format(table: Seq[Seq[String]]) = table match {
    case Seq( ) => ""
    case _ =>
      val sizes: Seq[Seq[Int]] = for (row <- table) yield for (cell <- row) yield
        if (cell == null) 0
        else {
          val text = cell.toString.replaceAllLiterally("""\u001b""", "").replaceAll(raw"\[\d+m", "")
          //          print(text + " - " + text.length + "\n")
          text.length
        }
      val colSizes = for (col <- sizes.transpose) yield if (minWidth > col.max) minWidth else col.max
      val rows = for (row <- table) yield formatRow(row, colSizes)
      formatRows(rowSeparator(colSizes), rows)
  }

  def formatRows(rowSeparator: String, rows: Seq[String]): String =
    (
    rowSeparator ::
      rows.head ::
      rowSeparator ::
      rows.tail.toList :::
      rowSeparator ::
      List( )).mkString("\n")

  def formatRow(row: Seq[String], colSizes: Seq[Int]) = {
    val cells = for ((item, size) <- row.zip(colSizes)) yield if (size == 0) "" else {

      val matchedSpecialChars = ("""\[\d+m""".r.findAllIn(item))
      item.formatted(s"%${
        if (size < minWidth) minWidth else size + matchedSpecialChars.aggregate(0)((
                                                                                   agg: Int,
                                                                                   str: String) => agg + str.length +
          1, (
                                                                                   i1, i2) => i1 + i2)
      }s")
    }

    cells.mkString("|", "|", "|")
  }

  def rowSeparator(colSizes: Seq[Int]) = colSizes map {
    "-" * _
  } mkString("+", "+", "+")
}
