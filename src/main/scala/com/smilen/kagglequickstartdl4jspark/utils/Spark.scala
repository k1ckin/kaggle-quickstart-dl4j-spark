package com.smilen.kagglequickstartdl4jspark.utils

import com.smilen.kagglequickstartdl4jspark.config.SparkGlobals._
import org.apache.spark.{SparkConf, SparkContext, sql}
import org.apache.spark.sql.SparkSession

object Spark {

  val sparkSession: SparkSession = new sql.SparkSession
  .Builder( )
    .config(sparkConfig)
    .getOrCreate( )
}
