package com.smilen.kagglequickstartdl4jspark.utils

import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.{Column, DataFrame}

object implicits {

  //    SAMPLE:
  implicit class MyDataFrame(df: DataFrame) {

    def firstCol(): Column = this.df(this.df.columns.head)

    def firstStruct(): StructField = this.df.schema.fields.head
  }

}
