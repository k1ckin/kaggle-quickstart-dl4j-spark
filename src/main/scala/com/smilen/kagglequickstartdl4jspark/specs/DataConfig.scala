package com.smilen.kagglequickstartdl4jspark.specs

import org.apache.spark.sql.types.{DataTypes, StructType}

object DataConfig {

  val trainSchema = new StructType( )
    .add("Field", DataTypes.StringType)

  val testSchema = new StructType( )
}
