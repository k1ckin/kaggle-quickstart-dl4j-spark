package com.smilen.kagglequickstartdl4jspark

import com.smilen.kagglequickstartdl4jspark.utils.Utils._

object Main {

  def main(args: Array[String]): Unit = {
    start
  }

  private def start = {
    readTrainDataAndRunQualityAnalysis
  }

  private def readTrainDataAndRunQualityAnalysis(): Unit = {
    val raw = readTrainCSVToDataFrame( )
    print(raw.describe( ))
    //    Flow.getQualityAnalysis(raw)
  }
}

