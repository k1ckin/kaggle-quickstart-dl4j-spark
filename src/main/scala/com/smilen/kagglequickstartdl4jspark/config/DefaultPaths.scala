package com.smilen.kagglequickstartdl4jspark.config

object DefaultPaths {

  val dataDirectory = "/data/"

  val trainCSVPath = dataDirectory + "train.csv"
}
