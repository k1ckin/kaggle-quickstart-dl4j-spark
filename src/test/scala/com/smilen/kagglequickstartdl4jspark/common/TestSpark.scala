package com.smilen.kagglequickstartdl4jspark.common

import org.apache.spark.sql.SparkSession
import org.apache.spark.{SparkConf, sql}

object TestSpark {

  val sparkSession: SparkSession = new sql.SparkSession
  .Builder( )
    .config(sparkConfig)
    .getOrCreate( )

  private def sparkConfig: SparkConf = new SparkConf( )
    .setMaster("local[*]")
    .setAppName("Test spark")
    .set("spark.driver.memory", "14G")
    //            .setExecutorEnv("spark.executor.memory","13G")
    .set("spark.sql.crossJoin.enabled", "true")
    .set("spark.network.timeout", "10000")
    .set("spark.driver.maxResultSize", "2G")
    .set("spark.memory.offHeap.size", "16g")
    .set("spark.memory.offHeap.enabled", "true")
    .setExecutorEnv("spark.executor.memoryOverhead", "1G")
    .set("spark.storage.memoryFraction", "0.1")
}
