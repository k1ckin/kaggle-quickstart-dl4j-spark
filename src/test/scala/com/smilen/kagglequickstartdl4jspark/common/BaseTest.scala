package com.smilen.kagglequickstartdl4jspark.common

import com.smilen.kagglequickstartdl4jspark.common.TestSpark.sparkSession
import org.apache.spark.sql.SparkSession

abstract class BaseTest {

  val spark: SparkSession = sparkSession
}
