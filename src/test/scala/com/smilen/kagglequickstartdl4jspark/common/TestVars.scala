package com.smilen.kagglequickstartdl4jspark.common

import org.apache.spark.sql.types.{DataTypes, StructType}

object TestVars {

  import TestSpark.{sparkSession => spark}

  val testResourcesURL = "src/test/resources/"
  val titanicURL = s"${testResourcesURL}titanic.csv"
  val titanicStruct: StructType = new StructType( )
    .add("PassengerId", DataTypes.IntegerType, false)
    .add("Survived", DataTypes.IntegerType, false)
    .add("Pclass", DataTypes.StringType, true)
    .add("Name", DataTypes.StringType, true)
    .add("Sex", DataTypes.StringType, true)
    .add("Age", DataTypes.DoubleType, false)
    .add("SibSp", DataTypes.IntegerType, true)
    .add("Parch", DataTypes.IntegerType, nullable = true)
    .add("Ticket", DataTypes.StringType, true)
    .add("Fare", DataTypes.DoubleType, true)
    .add("Cabin", DataTypes.StringType, true)
    .add("Embarked", DataTypes.StringType, true)

  val titanicDataset = spark.read
    .option("header", "true")
    .schema(titanicStruct)
    //    .option("timestampFormat", "MM-dd-yyyy hh mm ss")
    .csv(titanicURL)
}
