package com.smilen.kagglequickstartdl4jspark.datascience.analysis

import com.smilen.kagglequickstartdl4jspark.common.BaseTest
import com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnanalysis.ColumnAnalysisFunctions
import org.junit.{Assert, Test}

class DecimalAnalysisTest extends BaseTest {

  import spark.implicits._

  val testSet = Seq(1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0).toDF
  val analysis = ColumnAnalysisFunctions.getDataAnalysisForDecimalNumber(testSet)

  @Test
  def `Mean should equal average`() = {
    val mean = analysis.mean
    Assert.assertEquals(5.5, mean)
  }

  @Test
  def `Max should be the largest number`() = {
    val max = analysis.max
    Assert.assertEquals(10.0, max)
  }

  @Test
  def `Min should be the smallest number`() = {
    val min = analysis.min
    Assert.assertEquals(1.0, min)
  }

  @Test
  def `Standard deviation is correct`: Unit = {
    val stDev = analysis.stDev
    Assert.assertEquals(2.8722, stDev)
  }
}
