package com.smilen.kagglequickstartdl4jspark.datascience.analysis

import com.smilen.kagglequickstartdl4jspark.common.BaseTest
import com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnanalysis.ColumnAnalysisFunctions
import org.junit.{Assert, Test}

class StringAnalysisTest extends BaseTest {

  import spark.implicits._

  val testSet = Seq("string", "dog", "", "dog", "string", "string", "roomate", "parachuting", "abstractions")
  val df = testSet.toDF
  val analysis = ColumnAnalysisFunctions.getDataAnalysisForString(df)
  val stringLengthAnalysis = analysis.stringLengthAnalysis

  @Test
  def `Distinct should be correct`(): Unit = {
    val distinct = analysis.numberOfDistinct
    Assert.assertEquals(6L, distinct)
  }

  @Test
  def `Empty strings should be correct`(): Unit = {
    val empty = analysis.emptyStrings
    Assert.assertEquals(1L, empty)
  }

  // This particular tests and all others on the stringLengthAnalysis are already covered by WholeNumberTests.
  // We are merely passing the length of a string, still just a number
  @Test
  def `String length mean should be correct`(): Unit = {
    val mean = stringLengthAnalysis.mean
    val expected = testSet.map(_.length).sum / testSet.length.toDouble
    Assert.assertEquals(expected.formatted("%1.2f"), mean.formatted("%1.2f"))
  }

  @Test
  def `MostOccuring should come back correctly and sorted in descending order`(): Unit = {
    val mostOccuring = analysis.mostOccurring
    Assert.assertEquals(2, mostOccuring.length)
    Assert.assertEquals(("string", 3L), mostOccuring(0))
    Assert.assertEquals(("dog", 2L), mostOccuring(1))
  }
}
