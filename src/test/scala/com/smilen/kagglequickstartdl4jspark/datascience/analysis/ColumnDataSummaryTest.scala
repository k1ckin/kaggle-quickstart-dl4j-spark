package com.smilen.kagglequickstartdl4jspark.datascience.analysis

import com.smilen.kagglequickstartdl4jspark.common.BaseTest
import com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnanalysis.ColumnWholeNumberAnalysis
import com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnsummary.{
  ColumnDataSummaryFunctions,
  ColumnIntSummary
}
import org.apache.spark.sql.types.{IntegerType, StructField}
import org.junit.{Assert, Test}

class ColumnDataSummaryTest extends BaseTest {

  @Test
  def `getDataReportForAnalysis returns a IntegerReport for a WholeNumberAnalysis`(): Unit = {
    val dataAnalysis = new ColumnWholeNumberAnalysis(
      new StructField("asd", IntegerType, false)
      , 2, 3, 4.0, 5.0, 1)

    val report = ColumnDataSummaryFunctions.getDataSummaryForAnalysis(dataAnalysis)
    Assert.assertTrue(report.isInstanceOf[ColumnIntSummary])
  }
}
