package com.smilen.kagglequickstartdl4jspark.datascience.analysis

import com.smilen.kagglequickstartdl4jspark.common.BaseTest
import com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnanalysis.ColumnAnalysisFunctions
import org.junit._

class WholeNumberAnalysisTest extends BaseTest {

  import spark.implicits._

  val testSet = Seq(1, 2, 3, 4, 5, 6, 7, 8, 9, 10).toDF
  val analysis = ColumnAnalysisFunctions.getDataAnalysisForWholeNumber(testSet)

  @Test
  def `Mean should equal average`() = {
    val mean = analysis.mean
    Assert.assertEquals(5.5, mean)
  }

  @Test
  def `Max should be the largest number`() = {
    val max = analysis.max
    Assert.assertEquals(10L, max)
  }

  @Test
  def `Min should be the smallest number`() = {
    val min = analysis.min
    Assert.assertEquals(1L, min)
  }

  @Test
  def `Standard deviation is correct`: Unit = {
    val stDev = analysis.stdev
    Assert.assertEquals(2.8722, stDev)
  }
}
