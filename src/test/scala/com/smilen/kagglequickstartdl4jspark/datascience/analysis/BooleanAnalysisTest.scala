package com.smilen.kagglequickstartdl4jspark.datascience.analysis

import com.smilen.kagglequickstartdl4jspark.common.BaseTest
import com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnanalysis.ColumnAnalysisFunctions
import org.junit.{Assert, Test}

class BooleanAnalysisTest extends BaseTest {

  import spark.implicits._

  val df = Seq(true, false, true, false, false, false, true).toDF( )
  val analysis = ColumnAnalysisFunctions.getDataAnalysisForBoolean(df)

  @Test
  def `NumberOfTrueShouldBeCorrect`(): Unit = {
    Assert.assertEquals(3L, analysis.numberOfTrue)
  }

  @Test
  def `NumberOfFalseShouldBeCorrect`(): Unit = {
    Assert.assertEquals(4L, analysis.numberOfFalse)
  }

  @Test
  def `PercentageTrueShouldBeCorrect`(): Unit = {
    Assert.assertEquals((3L / 7.0).formatted("%1.2f"), analysis.percentageTrue.formatted("%1.2f"))
  }
}
