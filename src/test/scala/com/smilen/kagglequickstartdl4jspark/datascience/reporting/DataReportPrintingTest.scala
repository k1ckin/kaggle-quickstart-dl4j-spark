package com.smilen.kagglequickstartdl4jspark.datascience.reporting

import com.smilen.kagglequickstartdl4jspark.common.BaseTest
import com.smilen.kagglequickstartdl4jspark.datascience.analysis.DataReport
import com.smilen.kagglequickstartdl4jspark.datascience.analysis.columnsummary.{ColumnDataSummary, SummaryField}
import com.smilen.kagglequickstartdl4jspark.datascience.analysis.printing.console.ReportingToConsole
import org.junit.{Assert, Test}

class DataReportPrintingTest extends BaseTest {

  val intColumnDataReport = TestSummary("Column#1", "IntegerType", Seq(SummaryField("Max", "23.4"),
                                                                       SummaryField("Min", "15.0"),
                                                                       SummaryField("Avg", "20.4")))
  val expectedReport1Row1 = Seq("Name", "Type", "Max", "Min", "Avg")
  val expectedReport1Row2 = Seq("Column#1", "IntegerType", "23.4", "15.0", "20.4")

  val stringColumnDataReport = TestSummary("COl#2", "Stringish", Seq(SummaryField("Average", "5.8"),
                                                                     SummaryField("Longest", "15"),
                                                                     SummaryField("Shortest", "3")))
  val dataReport = DataReport(Seq(intColumnDataReport, stringColumnDataReport))
  val printableDataReport = ReportingToConsole.convertDataReportToConsolePrintableReport(dataReport)

  @Test
  def `Data Report should have have correct content`(): Unit = {
    Assert.assertEquals(2, printableDataReport.length)
    Assert.assertEquals(expectedReport1Row1, printableDataReport.head.head)
  }

  @Test
  def `Column Report should have have correct content`(): Unit = {
    val printableColumnReport = ReportingToConsole.convertColumnReportToConsolePrintable(intColumnDataReport)
    Assert.assertEquals(expectedReport1Row1, printableColumnReport.head)
    Assert.assertEquals(expectedReport1Row2, printableColumnReport(1))
    Assert.assertNotEquals(expectedReport1Row2, printableColumnReport.head)
    Assert.assertEquals("20.4", expectedReport1Row2.last)
  }

  @Test
  def `VISUAL - Report is printed fine`(): Unit = {
    ReportingToConsole.printConsolePrintableReport(printableDataReport)
  }

  case class TestSummary(
    override val columnName: String,
    override val columnType: String,
    override val reportFields: Seq[SummaryField]
  ) extends ColumnDataSummary

}
