package com.smilen.kagglequickstartdl4jspark.datascience.quality

import com.smilen.kagglequickstartdl4jspark.datascience.quality.ColumnTypeValidation._
import org.junit.Test

class TypeValidationTests {

  // Boolean
  @Test
  def `Non-boolean string should be not be valid Boolean` = {
    val invalidBooleans = "inv" :: "faaaal" :: "0.0" :: "1.0" :: Nil
    for (s <- invalidBooleans)
      assert(!isStringBoolean(s))
  }

  @Test
  def `Boolean string should be be valid Boolean` = {
    val validBooleans = "true" :: "false" :: "1" :: "0" :: Nil
    for (s <- validBooleans) assert(isStringBoolean(s))
  }

  // String
  @Test
  def `Empty string should be invalid`: Unit = {
    val invalidStrings = " " :: "" :: "  " :: Nil
    for (s <- invalidStrings) assert(!isStringValid(s))
  }

  @Test
  def `Valid string should be valid`: Unit = {
    val validStrings = " a" :: "qwew qqe" :: "123" :: Nil
    for (s <- validStrings) assert(isStringValid(s))
  }

  // Int & Long
  @Test
  def `Valid ints/longs should be valid`: Unit = {
    val validStrings = "12312312" :: "1123" :: "-23187890" :: "0" :: Nil
    for (s <- validStrings) assert(isStringIntOrLong(s), s)
  }

  @Test
  def `Invalid ints/longs should be invalid`: Unit = {
    val validStrings = "123123.12" :: "1123L" :: "2k3187890" :: "3231.3" :: "12e3" :: Nil
    for (s <- validStrings) assert(!isStringIntOrLong(s))
  }

  // Double & Float
  @Test
  def `Valid double/floats should be valid`: Unit = {
    val validStrings = "112441231" :: "1123" :: "-23187890" :: "0" :: "1000.00" :: "-1.3" :: "0.0" :: Nil
    for (s <- validStrings) assert(isStringDoubleOrFloat(s), s)
  }

  @Test
  def `Invalid double/floats should be invalid`: Unit = {
    val validStrings = "123123a.0" :: "a33" :: "--" :: "0a" :: "5.12L" :: "1000.00a" :: "-4*0.0" :: Nil
    for (s <- validStrings) assert(!isStringDoubleOrFloat(s), s)
  }

  // Timestamp
}
