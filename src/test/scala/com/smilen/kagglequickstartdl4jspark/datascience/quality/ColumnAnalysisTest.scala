package com.smilen.kagglequickstartdl4jspark.datascience.quality

import com.smilen.kagglequickstartdl4jspark.common.TestSpark.{sparkSession => spark}
import junit.framework.Assert
import org.apache.spark.sql.Row
import org.apache.spark.sql.types.{DataTypes, StructField, StructType}
import org.junit.Test

class ColumnAnalysisTest {

  val list = Seq(
    Row("1", "-1.5", "correct", "FALSE"),
    Row("2.1", "-2.5a", "", "wrong"),
    Row("-5", "10000", "right", "1"),
    Row(null, null, null, null)
  )

  val schema = new StructType( )
    .add(StructField("Int", DataTypes.StringType, true))
    .add(StructField("Double", DataTypes.StringType, true))
    .add(StructField("String", DataTypes.StringType, true))
    .add(StructField("Bool", DataTypes.StringType, true))

  val df = spark.createDataFrame(spark.sparkContext.parallelize(list), schema)

  @Test
  def `ColumnAnalysis - Column of Integers`(): Unit = {
    val analysis = QualityReporting.ColumnReporting
      .getColumnAnalysisForFirstColumn(df.select("Int"), StructField("ints", DataTypes.IntegerType))
    Assert.assertEquals(1L, analysis.invalid)
    Assert.assertEquals(1L, analysis.nulls)
  }

  @Test
  def `ColumnAnalysis - Column of Doubles`(): Unit = {
    val analysis = QualityReporting.ColumnReporting
      .getColumnAnalysisForFirstColumn(df.select("Double"), StructField("dblssss", DataTypes.DoubleType))
    Assert.assertEquals(1L, analysis.invalid)
    Assert.assertEquals(1L, analysis.nulls)
  }

  @Test
  def `ColumnAnalysis - Column of Strings`(): Unit = {
    val analysis = QualityReporting.ColumnReporting
      .getColumnAnalysisForFirstColumn(df.select("String"), StructField("dblssss", DataTypes.StringType))
    Assert.assertEquals(1L, analysis.invalid)
    Assert.assertEquals(1L, analysis.nulls)
  }

  @Test
  def `ColumnAnalysis - Column of Boolean`(): Unit = {
    val analysis = QualityReporting.ColumnReporting
      .getColumnAnalysisForFirstColumn(df.select("Bool"), StructField("dblssss", DataTypes.BooleanType))
    Assert.assertEquals(1L, analysis.invalid)
    Assert.assertEquals(1L, analysis.nulls)
  }
}
