package com.smilen.kagglequickstartdl4jspark.datascience.quality

import com.smilen.kagglequickstartdl4jspark.common.BaseTest
import com.smilen.kagglequickstartdl4jspark.utils.Tabulator
import org.apache.spark.sql.DataFrame
import org.junit.{Assert, BeforeClass, Test}
import com.smilen.kagglequickstartdl4jspark.common.TestVars.titanicDataset

class QualityReportTest extends BaseTest {

  val testDataset: DataFrame = titanicDataset
  val qualityReport: QualityReport = QualityReporting.getQualityReport(testDataset)

  @Test
  def `Quality Report - Correct number of totals`(): Unit = {
    Assert.assertEquals(891, qualityReport.totalRecords)
  }

  @Test
  def `Quality Report - Correct number of nulls`(): Unit = {
    val nullColumn = qualityReport.columnReports(5)
    Assert.assertEquals(177L, nullColumn.nulls)
    // column 6 (_c5) has 177 nulls
  }

  @Test
  def `Quality Report - Print table of summary`(): Unit = {
    print(Tabulator.format(qualityReport.getListOfColumnReportsPerFieldType( )))
  }
}